<?php

namespace Experiment\Bundle\RecipeFinderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Acme\DemoBundle\Form\ContactType;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use DateTime;

class RecipeController extends Controller
{
	/**
     * @Route("/hello/{name}", name="_demo_hello")
     * @Template()
     */
    public function helloAction($name)
    {
        return array('name' => $name);
    }

    /**
     * @Route("/getRecipe/{ingredients}/{recipes}", name="_get_recipe")
     * @Template()
     */
	public function getRecipeAction()
    {
    	$message = "Please provide complete file paths for a CSV file containing a list of ingredients and a JSON file containing a set recipes.";
    	$recommendedRecipe = "";

    	$request = Request::createFromGlobals();

    	$form = self::createFormBuilder()
            ->add('ingredients', 'text')
            ->add('recipes', 'text')
            ->add('get recipe', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
	        // data is an array with "ingredients" and "recipes" keys
	        $data = $form->getData();

	        // check that the specified files exist.
	        if (file_exists($data['ingredients']) && file_exists($data['recipes'])) {
	        	$ingredients = self::getIngredients($data['ingredients']);
	        	$recipes = self::getRecipes($data['recipes']);

	        	if ($ingredients !== false && $recipes !== false) {
	        		$recommendedRecipe = self::chooseRecipe($ingredients, $recipes);
	        	} else {
	        		$message = "Ingredients / Recipes could not be determined!";
	        	}
	        } else {
	        	// One of the specified files does not exist.
	        	$message = "Specified files do not exist.";	        	
	        }
	    }

		return array(
			'form' => $form->createView(),
			'message' => $message,
			'recommendedRecipe' => $recommendedRecipe
		);
    }

    /**
     * Generate an array of ingredients from a CSV file.
     *
     * @param str $ingredientsCsv The complete path to the ingredients CSV file.
     * @return array || bool
     */
    public function getIngredients($ingredientsCsv = null) 
    {
	    $ingredients = array();

    	if (!is_null($ingredientsCsv)) {

			// Loading CSV file and parsing the CSV contents.
			if (($handle = fopen($ingredientsCsv, "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				    $ingredients[] = $data;
				}
				fclose($handle);
			}
    	}

		return !empty($ingredients) ? $ingredients : false;
    }

    /**
     * Generate an array of reciepes from a JSON file.
     *
     * @param str $recipesJson The complete path to the recipes JSON file.
     * @return array || bool
     */
    public function getRecipes($recipesJson = null) 
    {
    	$recipes = array();

    	if (!is_null($recipesJson)) {
			$recipes = json_decode(file_get_contents($recipesJson), true);
    	}

		return !empty($recipes) ? $recipes : false;
	}

	/**
	 * Choose the best matching recipe based on the available ingredients and the use by 
	 * date of the individual ingredients.
	 *
	 * @param array $ingredients The full set of ingredients.
	 * @param array $recipes The full set of recipes.
	 * @return str $recommendedRecipe The recommended recipe name
	 */
	public function chooseRecipe($ingredients = array(), $recipes = array()) 
	{
		$recommendedRecipe = 'Order Takeout';

		if (!empty($ingredients) && !empty($recipes)) {
			// Clear out any expired ingredients.
			$ingredients = self::checkIngredientsUseBy($ingredients);

			// Generate a basic list of available ingredients.
			$availableIngredients = self::formatAvailableIngredients($ingredients);

			if ($ingredients !== false && $availableIngredients !== false) {
				$recipes = self::checkRecipes($recipes, $ingredients, $availableIngredients);
			} else {
				$recipes = array();
			}

			/**
			 * If there is more than one recipe to choose from, determine which recipe to select based on the fact 
			 * that the recipe with the nearest use by date should be recommended first. This can be achieved by 
			 * sorting the available recipes based on the use by date and selecting the first item in the list.
			 */
			if (!empty($recipes)) {
				if (count($recipes) > 1) {
					// Initiate a user defined sort to order the recipes by the use by date.
					usort($recipes, array($this,'compareDates'));
				}

				if (!empty($recipes)) {
					$recommendedRecipe = $recipes[0]['name'];
				}
			} else {
				return false;
			}
		} else {
			return false;
		}

		return $recommendedRecipe;
	}

	/**
     * Remove any ingredients that have passed the use by date.
     *
     * @param array $ingredients The array of ingredients.
     * @return array || bool
     */
	public function checkIngredientsUseBy($ingredients = array()) 
	{
		if (!empty($ingredients)) {
			/**
			 * Iterate through the list of ingredients and remove any that are past the 
			 * use by date.
			 */
			foreach ($ingredients as $ingredientKey => $ingredient) {
				$today = new DateTime();

				// Format the use by date as a valid date string that can be interpreted as a DateTime object.
				$useBy = new DateTime(str_replace('/', '-', $ingredient[3]));

				// If today's date is greater than the use by date, then remove the ingredient from the available options.
				if ($today > $useBy) {
					unset($ingredients[$ingredientKey]);
				}
			}
		} else {
			return false;
		}

		return $ingredients;
	}

	/**
     * Generate a basic list of available ingredients as a one dimensional array.
     *
     * Ensure that the keys are maintained as they can be used to help determine the use 
     * by date and therefore the recipe with the nearest use by.
     *
     * @param array $ingredients The array of ingredients.
     * @return array || bool
     */
	public function formatAvailableIngredients($ingredients = array()) 
	{
		$availableIngredients = array();
		
		if (!empty($ingredients)) {
			
			foreach ($ingredients as $ingredientKey => $ingredient) {
				$availableIngredients[$ingredientKey] = $ingredient[0];
			}
		} else {
			return false;
		}

		return !empty($availableIngredients) ? $availableIngredients : false;
	}

	/**
     * Generate an array of recipes than can be created using the available ingredients.
     *
     * Iterate through each recipe and determine if all necessary ingredients are 
	 * available.
     *
     * @param array $recipes The array of recipes.
     * @param array $ingredients The array of ingredients.
     * @param array $availableIngredients The one dimensional array of available ingredients.
     * @return array || bool
     */
	public function checkRecipes($recipes = array(), $ingredients = array(), $availableIngredients = array()) 
	{
		if (!empty($recipes) && !empty($ingredients) && !empty($availableIngredients)) {
			foreach ($recipes as $recipeKey => $recipe) {
				foreach ($recipe['ingredients'] as $recipeIngredientKey => $recipeIngredient) {
					// Determine the position of the current ingredient within the list of abailable ingredients.
					$ingredientPos = array_search($recipeIngredient['item'], $availableIngredients);

					if ($ingredientPos === false) {
						// The required ingredient is not available.
						unset($recipes[$recipeKey]);
						break;
					} else {
						/**
						 * Determine the use by date of the recipe by comparing the use by dates of the individual 
						 * ingredients. The nearest use by date should be set as the use by date of the recipe.
						 */
						if (!isset($recipes[$recipeKey]['useBy'])) {
							$recipes[$recipeKey]['useBy'] = $ingredients[$ingredientPos][3];
						} else {
							// Check if the use by date of the current ingredient is closer than the existing recipe use by date.
							$recipeUseBy = new DateTime(str_replace('/', '-', $recipes[$recipeKey]['useBy']));
							$ingredientUseBy = new DateTime(str_replace('/', '-', $ingredients[$ingredientPos][3]));

							// Use the nearest use by date to determine the use by date of the recipe.
							$recipes[$recipeKey]['useBy'] = $recipeUseBy < $ingredientUseBy ? $recipes[$recipeKey]['useBy'] : $ingredients[$ingredientPos][3]; 
						}
					}
				}
			}
		} else {
			return false;
		}

		return !empty($recipes) ? $recipes : false;
	}

	/**
	 * Date comparison function.
	 *
	 * @param array $a
	 * @param array $b
	 * @return int
	 */
	protected function compareDates($a, $b) 
	{
		// Create date objects for comparison
		$dateA = new DateTime(str_replace('/', '-', $a['useBy']));
	    $dateB = new DateTime(str_replace('/', '-', $b['useBy']));

	    if ($dateA == $dateB) {
	        return 0;
	    }

	    return ($dateA > $dateB) ? 1 : -1;
	}

}