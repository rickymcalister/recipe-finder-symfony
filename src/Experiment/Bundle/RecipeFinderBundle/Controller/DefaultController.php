<?php

namespace Experiment\Bundle\RecipeFinderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ExperimentRecipeFinderBundle:Default:index.html.twig', array('name' => $name));
    }
}
