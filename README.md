Recipe Finder - Symfony 2
=========================

Usage
-----

Run the built-in web server

	php app/console server:run

Visit [http://localhost:8000/getRecipe](http://localhost:8000/getRecipe) in a browser.

Enter the complete file paths to the ingredients CSV and recipes JSON files in the fields provided.

Bundle
------

The custom bundle can be found at *src/Experiment/RecipeFinderBundle/*